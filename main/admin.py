from django.contrib import admin

# Register your models here.
from .models import MataKuliah

class ContactFormAdmin(admin.ModelAdmin):
	class Meta:
		model = MataKuliah
		
admin.site.register(MataKuliah, ContactFormAdmin)