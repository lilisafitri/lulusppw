from django.shortcuts import render
from django.http import HttpResponseRedirect
from .forms import Input_Form


def home(request):
    return render(request, 'main/home.html')

def profile(request):
	return render(request, 'main/profile.html')

def project(request):
	return render(request, 'main/project.html')

def hobby(request):
	return render(request, 'main/hobby.html')

def experience(request):
	return render(request, 'main/experience.html')

def form(request):
	context={
		'input_form' : Input_Form,
	}
	return render (request, 'main/forms.html', context)

def matkul(request):
	'''
	return render(request, 'main/forms.html')
	'''
	form = Input_Form(request.POST or None)
	if (form.is_valid and request.method == "POST"):
		form.save()
	return render(request, 'forms.html', {'form': form})
