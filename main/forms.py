from django import forms
from .models import MataKuliah
from django.forms import ModelForm

class Input_Form(forms.ModelForm):
	class Meta:
		model = MataKuliah
		fields = [
		'nama', 
		'dosen', 
		'sks', 
		'deskripsi', 
		'tahun', 
		'kelas'
		]
	error_messages = {
		'required' : 'Please Type'
	}

	nama = forms.CharField(label='', required=False, max_length=100)

	dosen = forms.CharField(label='', required=False, max_length=100)

	sks = forms.CharField(label='', required=False, max_length=100)

	deskripsi = forms.CharField(label='', required=False, max_length=100)

	tahun = forms.CharField(label='', required=False, max_length=100)

	kelas = forms.CharField(label='', required=False, max_length=100)

