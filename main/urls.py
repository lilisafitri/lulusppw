from django.urls import path

from . import views

app_name = 'main'

urlpatterns = [
    path('', views.home, name='home'),
    path('profile/', views.profile, name='profile'),
    path('hobby/', views.hobby, name='hobby'),
    path('project/', views.project, name='project'),
    path('experience/', views.experience, name='experience'),

    path('matakuliah/', views.form, name='form'),
    path('form/matakuliah', views.form, name='form')
]
